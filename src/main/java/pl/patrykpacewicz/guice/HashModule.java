package pl.patrykpacewicz.guice;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class HashModule extends AbstractModule {
    @Override
    protected void configure() {
        Names.bindProperties(binder(), loadProperties("application.properties"));
    }

    private Properties loadProperties(String filename) {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }


}
