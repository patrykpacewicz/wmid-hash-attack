package pl.patrykpacewicz.utils;

public class Bit {
    public boolean isLastBitSet(byte[] arr) {
        return (arr[arr.length-1] % 2 ) == 1;
    }

    public char changeLastBit(char x) {
        return (char)(x ^ 0x1);
    }
}
