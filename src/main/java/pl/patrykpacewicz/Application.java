package pl.patrykpacewicz;

import com.google.inject.Guice;
import pl.patrykpacewicz.guice.HashModule;
import pl.patrykpacewicz.hash.attack.CollisionAttack;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.FileInputStream;

public class Application {
    private final CollisionAttack collisionAttack;
    private final String filePathFirst;
    private final String filePathSecond;

    @Inject
    public Application(
        CollisionAttack collisionAttack,
        @Named("hash.file.good") String filePathFirst,
        @Named("hash.file.evil") String filePathSecond
    ) {
        this.collisionAttack = collisionAttack;
        this.filePathFirst   = filePathFirst;
        this.filePathSecond  = filePathSecond;
    }

    public static void main(String[] args) {
        Guice.createInjector(new HashModule()).getInstance(Application.class).run();
    }

    public void run() {
        try {
            collisionAttack.attack(new FileInputStream(filePathFirst), new FileInputStream(filePathSecond));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
