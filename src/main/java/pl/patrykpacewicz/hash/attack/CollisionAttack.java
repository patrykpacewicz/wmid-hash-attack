package pl.patrykpacewicz.hash.attack;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.IOUtils;
import pl.patrykpacewicz.hash.Hash;
import pl.patrykpacewicz.utils.Bit;
import pl.patrykpacewicz.utils.ByteArrayBitIterable;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CollisionAttack {
    private final Integer fileFirstCharToChange;
    private final long loopNumber;
    private final Hash hash;
    private final HashContainerSwapper hashSwapper;
    private final Bit bit;

    private byte[] tmpLastHash = new byte[]{};
    private long bigIterations = 0;

    @Inject
    public CollisionAttack(
        @Named("hash.file.first.char.to.change") Integer fileFirstCharToChange,
        @Named("hash.bit.length") Integer hashBitLength,
        HashContainerSwapper hashSwapper,
        Bit bit,
        Hash hash
    ) {
        this.hashSwapper = hashSwapper;
        this.bit = bit;
        this.fileFirstCharToChange = fileFirstCharToChange - 1;
        this.loopNumber = (long) Math.pow(2, hashBitLength/2);
        this.hash = hash;
    }

    public void attack(InputStream goodInput, InputStream evilInput) throws IOException {
        char[] messageGood = IOUtils.toCharArray(goodInput);
        char[] messageEvil = IOUtils.toCharArray(evilInput);

        hashSwapper.getGood().addNewHashBrakePoint(messageGood);
        hashSwapper.getEvil().addNewHashBrakePoint(messageEvil);
        debug(messageGood);
        debug(messageEvil);

        tmpLastHash = hashSwapper.setGoodActive().getGood().getLastBrakePointHashAsByteArray();

        while (!hashSwapper.hasFoundConflict()) {
            bigIterations++;
            findCollision();
        }

        byte[] tmpHash  = hashSwapper.getInactive().getLastConflictHashAsByteArray();
        char[] tmpInput = hashSwapper.getInactive().getLastConflictMessage();

        for (long i = 0; i < loopNumber; i++) {
            tmpInput = modifyMessage(tmpHash, tmpInput);
            tmpHash = hash.data2HashByte(tmpInput);

            if (Hex.encodeHexString(tmpHash).equals(hashSwapper.getActive().getLastBrakePointHash())) {
                hashSwapper.getInactive().addNewHashBrakePoint(tmpInput);
                break;
            }
        }

        printResult();
    }

    private void findCollision() {
        if (bit.isLastBitSet(tmpLastHash)) {
            hashSwapper.setGoodActive();
        } else {
            hashSwapper.setEvilActive();
        }

        byte[] tmpHash  = hashSwapper.getActive().getLastBrakePointHashAsByteArray();
        char[] tmpInput = hashSwapper.getActive().getLastBrakePointMessage();

        tmpLastHash = tmpHash;

        for (long i = 0; i < loopNumber; i++) {
            tmpInput = modifyMessage(tmpHash, tmpInput);
            tmpHash = hash.data2HashByte(tmpInput);

            hashSwapper.getActive().addToAllHashesList(tmpHash);

            if (validate(tmpHash, tmpInput)) {
                debug(tmpInput);
                return;
            }
        }

        hashSwapper.getActive().addNewHashBrakePoint(tmpInput);
        debug(tmpInput);
    }

    private char[] modifyMessage(byte[] tmpHash, char[] tmpInput) {
        int j = fileFirstCharToChange;
        for (boolean msgBit : new ByteArrayBitIterable(tmpHash)) {
            if (msgBit) {
                tmpInput[j] = bit.changeLastBit(tmpInput[j]);
            }
            j++;
        }

        return tmpInput;
    }

    private void debug(char[] tmpInput) {
        System.out.print(bigIterations + " ");
        System.out.print(hashSwapper.getGood().getHashBrakePoints().size() + ":" + hashSwapper.getEvil().getHashBrakePoints().size() + " ");
        System.out.print(hash.data2HashString(tmpInput) + " ");
        System.out.print(hashSwapper.getActive().equals(hashSwapper.getGood())? "GOOD": "EVIL");
        System.out.println();
    }

    private boolean validate(byte[] tmpHash, char[] tmpInput) {
        if (!hashSwapper.getInactive().isConflict(tmpHash)) {
            return false;
        }

        hashSwapper.getActive().addNewHashBrakePoint(tmpInput);
        return true;
    }

    private void printResult() {
        List<char[]> commonHashMap = hashSwapper.getConflictResults();
        for (char[] messages : commonHashMap) {
            System.out.println("=====================================");
            System.out.println("Hash: " + hash.data2HashString(messages));
            System.out.println("Message: ");
            System.out.println(messages);
        }
    }
}
