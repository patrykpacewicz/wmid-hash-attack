package pl.patrykpacewicz.hash.attack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import pl.patrykpacewicz.hash.Hash;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HashContainer {
    private final Hash hash;

    private final Map<String, char[]> hashBrakePoints = Maps.newHashMap();
    private final List<List<String>> allHashes = Lists.newArrayList();

    private String lastBrakePointHash;
    private char[] lastBrakePointMessage;

    private String lastConflictHash;
    private char[] lastConflictMessage;

    @Inject
    public HashContainer(Hash hash) {
        this.hash = hash;
    }

    public void addNewHashBrakePoint(char[] message) {
        lastBrakePointMessage = message.clone();
        lastBrakePointHash = hash.data2HashString(lastBrakePointMessage);

        hashBrakePoints.put(lastBrakePointHash, lastBrakePointMessage.clone());
        addToAllHashesList(lastBrakePointHash);
    }

    public void addToAllHashesList(byte[] hash) {
        addToAllHashesList(Hex.encodeHexString(hash));
    }

    public void addToAllHashesList(String hash) {
        int index = hashBrakePoints.size();
        if (allHashes.size() < index) {
            allHashes.add(new ArrayList<String>());
        }

        allHashes.get(index - 1).add(hash);
    }

    public boolean isConflict(byte[] hash) {
        return isConflict(Hex.encodeHexString(hash));
    }

    public boolean isConflict(String hash) {
        for (List<String> hashesList : allHashes) {
            for (String checkHash : hashesList) {
                if (checkHash.equals(hash)) {
                    lastConflictHash = hashesList.get(0);
                    lastConflictMessage = hashBrakePoints.get(lastConflictHash);
                    return true;
                }
            }
        }

        return false;
    }

    public boolean hasFoundConflict() {
        return lastConflictHash != null && !lastConflictHash.isEmpty();
    }

    public Map<String, char[]> getHashBrakePoints() {
        return hashBrakePoints;
    }

    public String getLastBrakePointHash() {
        return lastBrakePointHash;
    }

    public char[] getLastBrakePointMessage() {
        return lastBrakePointMessage.clone();
    }

    public String getLastConflictHash() {
        return lastConflictHash;
    }

    public char[] getLastConflictMessage() {
        return lastConflictMessage;
    }

    public byte[] getLastBrakePointHashAsByteArray() {
        return hash.hashString2HashByte(getLastBrakePointHash()).clone();
    }

    public byte[] getLastConflictHashAsByteArray() {
        return hash.hashString2HashByte(getLastConflictHash()).clone();
    }
}
