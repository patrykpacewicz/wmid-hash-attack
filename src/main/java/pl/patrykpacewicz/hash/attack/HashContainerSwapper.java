package pl.patrykpacewicz.hash.attack;

import com.google.common.collect.Lists;

import javax.inject.Inject;
import java.util.List;

class HashContainerSwapper {
    private final HashContainer containerGood;
    private final HashContainer containerEvil;
    private HashContainer activeContainer;
    private HashContainer inactiveContainer;

    @Inject
    public HashContainerSwapper(HashContainer containerGood, HashContainer containerEvil) {
        this.containerGood = containerGood;
        this.containerEvil = containerEvil;

        this.setGoodActive();
    }

    public boolean hasFoundConflict() {
        return containerGood.hasFoundConflict() || containerEvil.hasFoundConflict();
    }

    public List<char[]> getConflictResults() {
        List<char[]> result = Lists.newArrayList();

        for (String hash : containerGood.getHashBrakePoints().keySet()) {
            if (containerEvil.getHashBrakePoints().containsKey(hash)) {
                result.add(containerGood.getHashBrakePoints().get(hash));
                result.add(containerEvil.getHashBrakePoints().get(hash));
                return result;
            }
        }

        return result;
    }

    public HashContainerSwapper setGoodActive() {
        activeContainer   = containerGood;
        inactiveContainer = containerEvil;
        return this;
    }

    public HashContainerSwapper setEvilActive() {
        activeContainer   = containerEvil;
        inactiveContainer = containerGood;
        return this;
    }

    public HashContainer getGood() {
        return containerGood;
    }

    public HashContainer getEvil() {
        return containerEvil;
    }

    public HashContainer getActive() {
        return activeContainer;
    }

    public HashContainer getInactive() {
        return inactiveContainer;
    }
}
