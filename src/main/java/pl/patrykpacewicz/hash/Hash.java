package pl.patrykpacewicz.hash;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import pl.patrykpacewicz.hash.exception.HashString2HashByteException;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.Arrays;

public class Hash {
    private final Integer hashBitLength;

    @Inject
    public Hash(@Named("hash.bit.length") Integer hashBitLength) {
        this.hashBitLength = hashBitLength;
    }

    public String data2HashString(final char[] data) {
        return substring(Hex.encodeHexString(getHash(data)));
    }

    public byte[] data2HashByte(final char[] data) {
        return subarray(getHash(data));
    }

    public String hashByte2HashString(byte[] hash) {
        return Hex.encodeHexString(hash);
    }

    public byte[] hashString2HashByte(String hash) {
        try {
            return Hex.decodeHex(hash.toCharArray());
        } catch (DecoderException ex) {
            throw new HashString2HashByteException(ex);
        }
    }

    private byte[] getHash(char[] data) {
        return DigestUtils.md5(new String(data).getBytes());
    }

    private String substring(String data) {
        return data.substring(0, hashBitLength/8*2);
    }

    private byte[] subarray(byte [] data) {
        return Arrays.copyOfRange(data, 0, hashBitLength/8);
    }
}
